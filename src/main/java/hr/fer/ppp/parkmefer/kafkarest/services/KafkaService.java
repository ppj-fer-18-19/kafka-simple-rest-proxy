package hr.fer.ppp.parkmefer.kafkarest.services;

import hr.fer.ppp.parkmefer.kafkarest.dto.MessageDTO;

public interface KafkaService {

    void registerIO(MessageDTO messageDTO);
}
