package hr.fer.ppp.parkmefer.kafkarest.services.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import hr.fer.ppp.parkmefer.kafkarest.dto.MessageDTO;
import hr.fer.ppp.parkmefer.kafkarest.services.KafkaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.util.Date;

@Service
public class KafkaServiceImpl implements KafkaService {

    private static final Gson GSON = new GsonBuilder().setDateFormat(DateFormat.FULL, DateFormat.FULL).create();
    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaServiceImpl.class);

    @Value("${app.kafka.topic}")
    private String topic;

    private KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    public KafkaServiceImpl(KafkaTemplate<String, String> kafkaTemplate){
        this.kafkaTemplate = kafkaTemplate;
    }

    @Override
    public void registerIO(MessageDTO messageDTO) {
        messageDTO.setTime(new Date());
        String kafkaEntryMessage = GSON.toJson(messageDTO);
        kafkaTemplate.send(topic, kafkaEntryMessage);

        LOGGER.info(messageDTO.toString());
    }
}
