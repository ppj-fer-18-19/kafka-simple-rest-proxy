package hr.fer.ppp.parkmefer.kafkarest.controllers;

import hr.fer.ppp.parkmefer.kafkarest.dto.MessageDTO;
import hr.fer.ppp.parkmefer.kafkarest.services.KafkaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("io")
public class IOController {

    private final KafkaService kafkaService;

    @Autowired
    public IOController(KafkaService kafkaService) {
        this.kafkaService = kafkaService;
    }

    @PostMapping(path = "/", produces = "application/json")
    public void registerIO(@RequestBody @Valid MessageDTO messageDTO){
        kafkaService.registerIO(messageDTO);
    }
}
